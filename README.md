# HPM-Frame: The case study

This repository contains the case study from the paper *HPM-Frame: A decision
framework for executing software on heterogeneous platforms* [1]. The case study 
investigates the process of refactoring a CPU-based edge-detector into a
Vulkan-based GPU version using the framework. During the process, the questions
posed within the framework were reflected upon and answered from the
perspective of a real-world case. Please refer to the paper for the detailed
answers. The two software components were implemented as OpenDLV microservices.

[1]: Hugo Andrade, Ola Benderius, Christian Berger, Ivica Crnkovic, Jan Bosch.
HPM-Frame: A decision framework for executing software on heterogeneous
platforms. Journal of Systems and Software (submitted)

## Build and run

Docker is used to build and run the software. However, the GPU version does not
run well with Docker as drivers need to match between the host and the container.
Therefore, the below steps show how to test the GPU implementation manually.

### Test the CPU version

Dependencies:
* Docker
* docker-compose

The CPU version as well as the data replay is fully containerized, and can be
tested in the following way:
```bash
xhost +
docker-compose up
```
The example uses a video log included in the repository, and the microservice
for data replay is part of the OpenDLV framework. Note: The h264 replay is
compiled before being automatically started (due to licensing reasons).

### Test the GPU version

Dependencies:
* Vulkan
* glslangValidator
* cmake
* xxd
* X11

The GPU version can be built using CMake. Vulkan is the main dependency for the
microservice. Typically the Vulkan mesa driver (for Intel, ATI, or Nvidia),
the Vulkan loader, the Vulkan headers, and the Vulkan validation layers are
needed. The glslangValidator and the xxd tools are automatically used inside the
build process for the purpose of including the compiled compute
shader into the source code. Please refer to the documentation from your Linux
distribution for further instructions on how to install the dependencies.

When all dependencies are installed, the GPU-based edge detection can be
compiled in the following way:
```bash
mkdir opendlv-perception-vision-convolution-gpu/build
cd opendlv-perception-vision-convolution-gpu/build
cmake ..
make
```

Then, open another terminal and start the data replay (e.g. `docker-compose up video-h264-replay`), before running:
```bash
./opendlv-perception-vision-convolution-gpu --name=img.argb --width=1280 --height=720 --gpu-id=0 --verbose
```

## Evaluation of performance

When evaluating performance, make sure to remove the `--verbose` flags, to
suppress debugging output and visualization.

Typical values for full per-frame convolutions, measured from 300 frames, for an
Intel graphics card are:

|     | Mean (ms) | Std (ms) |
|:----|----------:|---------:|
| CPU | 12.52     | 3.89     |
| GPU |  4.73     | 1.68     |

