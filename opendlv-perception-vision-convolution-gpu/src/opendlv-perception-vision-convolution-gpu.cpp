/*
 * Copyright (C) 2020 Ola Benderius
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <memory>
#include <mutex>

#include <vulkan/vulkan.h>
#include <X11/Xlib.h>

#include "cluon-complete.hpp"
#include "comp.spv.hpp"

std::vector<const char*> const validationLayers = {
  "VK_LAYER_KHRONOS_validation"
};

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT,
    VkDebugUtilsMessageTypeFlagsEXT,
    VkDebugUtilsMessengerCallbackDataEXT const *pCallbackData, void *)
{
  std::cerr << "Validation layer: " << pCallbackData->pMessage << std::endl;
  return VK_FALSE;
}

int32_t main(int32_t argc, char **argv) {
  auto commandlineArguments = cluon::getCommandlineArguments(argc, argv);
  if ((0 == commandlineArguments.count("name")) ||
      (0 == commandlineArguments.count("width")) ||
      (0 == commandlineArguments.count("height"))) {
    std::cerr << argv[0] << " attaches to a shared memory area containing an "
      << "ARGB image to find edges using convolution." << std::endl;
    std::cerr << "Usage:   " << argv[0] << " <OPTION>" << std::endl;
    std::cerr << "  --name:   name of the shared memory area to attach"
      << std::endl;
    std::cerr << "  --width:  width of the frame" << std::endl;
    std::cerr << "  --height: height of the frame" << std::endl;
    std::cerr << "  --gpu-id: index of GPU (default: 0)" << std::endl;
    std::cerr << "Example: " << argv[0] << " --name=img.argb --width=1280 "
      << "--height=720 --verbose" << std::endl;
    return 0;
  }

  std::string const name{commandlineArguments["name"]};
  uint32_t const width{static_cast<uint32_t>(
      std::stoi(commandlineArguments["width"]))};
  uint32_t const height{static_cast<uint32_t>(
      std::stoi(commandlineArguments["height"]))};
  uint32_t const gpuIndexPreferred{commandlineArguments.count("gpu-id") != 0 ?
    static_cast<uint32_t>(std::stoi(commandlineArguments["gpu-id"])) : 0};
  bool const verbose{commandlineArguments.count("verbose") != 0};

  if (width != 1280 || height != 720) {
    std::cerr << "Image resolution needs to be 1280x720 due to static shader."
      << std::endl;
    return -1;
  }

  uint32_t const bufferSize{4 * width * height};
  uint8_t image[bufferSize];

  Display* display{nullptr};
  Visual* visual{nullptr};
  Window window{0};
  XImage* ximage{nullptr};
  
  VkDeviceSize const memorySize = 2 * bufferSize;

  VkInstance instance;
  VkDevice device;
  VkDeviceMemory memory;
  VkBuffer inBuffer;
  VkBuffer outBuffer;
  VkShaderModule shaderModule;
  VkDescriptorSetLayout descriptorSetLayout;
  VkPipelineLayout pipelineLayout;
  VkPipeline pipeline;
  VkDescriptorPool descriptorPool;
  VkCommandPool commandPool;

  auto destroyVulkan = [&instance, &device, &memory, &inBuffer, &outBuffer,
  &shaderModule, &descriptorSetLayout, &pipelineLayout, &pipeline,
  &descriptorPool, &commandPool]() {
    vkDestroyCommandPool(device, commandPool, nullptr);
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
    vkDestroyPipeline(device, pipeline, nullptr);
    vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
    vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
    vkDestroyShaderModule(device, shaderModule, nullptr);
    vkDestroyBuffer(device, outBuffer, nullptr);
    vkDestroyBuffer(device, inBuffer, nullptr);
    vkFreeMemory(device, memory, nullptr);
    vkDestroyDevice(device, nullptr);
    vkDestroyInstance(instance, nullptr);
  };

  {
    VkApplicationInfo const applicationInfo = {
      VK_STRUCTURE_TYPE_APPLICATION_INFO,
      0,
      "opendlv-perception-vision-convolution",
      1,
      "",
      0,
      VK_API_VERSION_1_2
    };
    VkInstanceCreateInfo instanceCreateInfo = {
      VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      0,
      0,
      &applicationInfo,
      0,
      0,
      0,
      0
    };
    VkDebugUtilsMessengerCreateInfoEXT const debugCreateInfo = {
      VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
      0,
      0,
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
        | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
        | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
      VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
        | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
        | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
      debugCallback,
      0
    };

    if (verbose) {
      instanceCreateInfo.enabledLayerCount =
        static_cast<uint32_t>(validationLayers.size());
      instanceCreateInfo.ppEnabledLayerNames = validationLayers.data();
      instanceCreateInfo.pNext =
        (VkDebugUtilsMessengerCreateInfoEXT *) &debugCreateInfo;
    }

    VkResult res = vkCreateInstance(&instanceCreateInfo, 0, &instance);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not create Vulkan instance." << std::endl;
      return -1;
    }
  }
  
  uint32_t physicalDeviceCount{0};
  {
    VkResult res = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount,
        0);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not enumerate capable devices." << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  std::vector<VkPhysicalDevice> physicalDevices(physicalDeviceCount);
  {
    VkResult res = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount,
        physicalDevices.data());
    if (res != VK_SUCCESS) {
      std::cerr << "Could not enumerate capable devices." << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  uint32_t gpuIndex{0};

  std::cout << "GPUs found: " << std::endl;
  for (uint32_t i = 0; i < physicalDeviceCount; i++) {
    VkPhysicalDeviceProperties physical_properties = {};
    vkGetPhysicalDeviceProperties(physicalDevices[i], &physical_properties);
    if (gpuIndexPreferred == i) {
      std::cout << "  [" << i << "*] ";
      gpuIndex = i;
    } else {
      std::cout << "  [" << i << " ] ";
    }
    std::cout << physical_properties.deviceName << std::endl;
  }
  if (gpuIndex != gpuIndexPreferred) {
    std::cout << "Warning: Could not select preferred GPU. Selected "
      << gpuIndex << std::endl;
  }

  uint32_t queueFamilyIndex{0};
  {
    uint32_t queueFamilyPropertiesCount{0};
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevices[gpuIndex],
        &queueFamilyPropertiesCount, 0);

    std::vector<VkQueueFamilyProperties> queueFamilyProperties(
        queueFamilyPropertiesCount);

    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevices[gpuIndex],
        &queueFamilyPropertiesCount, queueFamilyProperties.data());

    bool foundQueue{false};
    for (uint32_t i{0}; i < queueFamilyPropertiesCount; i++) {
      VkQueueFlags const maskedFlags = 
        (~(VK_QUEUE_TRANSFER_BIT | VK_QUEUE_SPARSE_BINDING_BIT) &
         queueFamilyProperties[i].queueFlags);

      if (!(VK_QUEUE_GRAPHICS_BIT & maskedFlags) &&
          (VK_QUEUE_COMPUTE_BIT & maskedFlags)) {
        queueFamilyIndex = i;
        foundQueue = true;
        break;
      }
    }

    if (!foundQueue) {
      for (uint32_t i{0}; i < queueFamilyPropertiesCount; i++) {
        VkQueueFlags const maskedFlags =
          (~(VK_QUEUE_TRANSFER_BIT | VK_QUEUE_SPARSE_BINDING_BIT) &
           queueFamilyProperties[i].queueFlags);

        if (VK_QUEUE_COMPUTE_BIT & maskedFlags) {
          queueFamilyIndex = i;
          foundQueue = true;
          break;
        }
      }
    }

    if (!foundQueue) {
      std::cerr << "Could not select any compute queue family." << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  {
    float const queuePrioritory{1.0f};
    VkDeviceQueueCreateInfo const deviceQueueCreateInfo = {
      VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
      0,
      0,
      queueFamilyIndex,
      1,
      &queuePrioritory
    };
    VkDeviceCreateInfo const deviceCreateInfo = {
      VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      0,
      0,
      1,
      &deviceQueueCreateInfo,
      0,
      0,
      0,
      0,
      0
    };
    VkResult res = vkCreateDevice(physicalDevices[gpuIndex], &deviceCreateInfo,
        0, &device);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not create logical device" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  {
    VkPhysicalDeviceMemoryProperties properties;
    vkGetPhysicalDeviceMemoryProperties(physicalDevices[gpuIndex], &properties);

    uint32_t memoryTypeIndex = VK_MAX_MEMORY_TYPES;
    for (uint32_t i{0}; i < properties.memoryTypeCount; i++) {
      if ((VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT 
            & properties.memoryTypes[i].propertyFlags)
          && (VK_MEMORY_PROPERTY_HOST_COHERENT_BIT 
            & properties.memoryTypes[i].propertyFlags)
          && (memorySize 
            < properties.memoryHeaps[properties.memoryTypes[i].heapIndex].size)) {
        memoryTypeIndex = i;
        break;
      }
    }
    if (memoryTypeIndex == VK_MAX_MEMORY_TYPES) {
      std::cerr << "Could not find GPU with required memory" << std::endl;
      destroyVulkan();
      return -1;
    }

    VkMemoryAllocateInfo const memoryAllocateInfo = {
        VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        0,
        memorySize,
        memoryTypeIndex
      };

    VkResult res = vkAllocateMemory(device, &memoryAllocateInfo, 0, &memory);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not allocate GPU memory" << std::endl;
      destroyVulkan();
      return -1;
    }
  }
  
  {
    VkBufferCreateInfo const bufferCreateInfo = {
      VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
      0,
      0,
      bufferSize,
      VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
      VK_SHARING_MODE_EXCLUSIVE,
      1,
      &queueFamilyIndex
    };
    {
      VkResult res = vkCreateBuffer(device, &bufferCreateInfo, 0, &inBuffer);
      if (res != VK_SUCCESS) {
        std::cerr << "Could not create input buffer" << std::endl;
        destroyVulkan();
        return -1;
      }
    }
    {
      VkResult res = vkBindBufferMemory(device, inBuffer, memory, 0);
      if (res != VK_SUCCESS) {
        std::cerr << "Could not bind input buffer" << std::endl;
        destroyVulkan();
        return -1;
      }
    }
    {
      VkResult res = vkCreateBuffer(device, &bufferCreateInfo, 0, &outBuffer);
      if (res != VK_SUCCESS) {
        std::cerr << "Could not create output buffer" << std::endl;
        destroyVulkan();
        return -1;
      }
    }
    {
      VkResult res = vkBindBufferMemory(device, outBuffer, memory, bufferSize);
      if (res != VK_SUCCESS) {
        std::cerr << "Could not bind input buffer" << std::endl;
        destroyVulkan();
        return -1;
      }
    }
  }

  {
    std::vector<uint8_t> shader(comp_spv, comp_spv + comp_spv_len);

    VkShaderModuleCreateInfo const shaderModuleCreateInfo = {
      VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
      0,
      0,
      shader.size(),
      reinterpret_cast<uint32_t const *>(shader.data())
    };

    VkResult res = vkCreateShaderModule(device, &shaderModuleCreateInfo, 0,
        &shaderModule);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not create shader" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  {
    VkDescriptorSetLayoutBinding const descriptorSetLayoutBindings[2] = {
      {
        0,
        VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        1,
        VK_SHADER_STAGE_COMPUTE_BIT,
        0
      },
      {
        1,
        VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        1,
        VK_SHADER_STAGE_COMPUTE_BIT,
        0
      }
    };
    VkDescriptorSetLayoutCreateInfo const descriptorSetLayoutCreateInfo = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
      0,
      0,
      2,
      descriptorSetLayoutBindings
    };
    VkResult res = vkCreateDescriptorSetLayout(device,
        &descriptorSetLayoutCreateInfo, 0, &descriptorSetLayout);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not create descriptor set layout" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  {
    VkPipelineLayoutCreateInfo const pipelineLayoutCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      0,
      0,
      1,
      &descriptorSetLayout,
      0,
      0
    };
    VkResult res = vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, 0,
        &pipelineLayout);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not create pipeline layout" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  {
    VkPipelineShaderStageCreateInfo const pipelineShaderStageCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      0,
      0,
      VK_SHADER_STAGE_COMPUTE_BIT,
      shaderModule,
      "main",
      0
    };
    VkComputePipelineCreateInfo const computePipelineCreateInfo = {
      VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
      0,
      0,
      pipelineShaderStageCreateInfo,
      pipelineLayout,
      0,
      0
    };
    VkResult res = vkCreateComputePipelines(device, 0, 1,
        &computePipelineCreateInfo, 0, &pipeline);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not create pipeline" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  {
    VkDescriptorPoolSize const descriptorPoolSize = {
      VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
      2
    };
    VkDescriptorPoolCreateInfo const descriptorPoolCreateInfo = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
      0,
      0,
      1,
      1,
      &descriptorPoolSize
    };
    VkResult res = vkCreateDescriptorPool(device, &descriptorPoolCreateInfo, 0,
        &descriptorPool);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not create descriptor pool" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  VkDescriptorSet descriptorSet;
  {
    VkDescriptorSetAllocateInfo const descriptorSetAllocateInfo = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
      0,
      descriptorPool,
      1,
      &descriptorSetLayout
    };
    VkResult res = vkAllocateDescriptorSets(device, &descriptorSetAllocateInfo,
        &descriptorSet);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not allocate descriptor sets" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  {
    VkDescriptorBufferInfo const inDescriptorBufferInfo = {
      inBuffer,
      0,
      VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo const outDescriptorBufferInfo = {
      outBuffer,
      0,
      VK_WHOLE_SIZE
    };
    VkWriteDescriptorSet const writeDescriptorSet[2] = {
      {
        VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        0,
        descriptorSet,
        0,
        0,
        1,
        VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        0,
        &inDescriptorBufferInfo,
        0
      },
      {
        VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        0,
        descriptorSet,
        1,
        0,
        1,
        VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        0,
        &outDescriptorBufferInfo,
        0
      }
    };
    vkUpdateDescriptorSets(device, 2, writeDescriptorSet, 0, 0);
  }

  {
    VkCommandPoolCreateInfo const commandPoolCreateInfo = {
      VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      0,
      0,
      queueFamilyIndex
    };
    VkResult res = vkCreateCommandPool(device, &commandPoolCreateInfo, 0,
        &commandPool);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not create command pool" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  VkCommandBuffer commandBuffer;
  {
    VkCommandBufferAllocateInfo const commandBufferAllocateInfo = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      0,
      commandPool,
      VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      1
    };
    VkResult res = vkAllocateCommandBuffers(device, &commandBufferAllocateInfo,
        &commandBuffer);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not allocate command buffers" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  {
    VkCommandBufferBeginInfo const commandBufferBeginInfo = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      0,
      VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
      0
    };
    VkResult res = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not begin command buffer" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);

  vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE,
      pipelineLayout, 0, 1, &descriptorSet, 0, 0);

  // Local size 16x16
  vkCmdDispatch(commandBuffer, width / 16, height / 16, 1);

  {
    VkResult res = vkEndCommandBuffer(commandBuffer);
    if (res != VK_SUCCESS) {
      std::cerr << "Could not end command buffer" << std::endl;
      destroyVulkan();
      return -1;
    }
  }

  if (verbose) {
    display = XOpenDisplay(nullptr);
    visual = DefaultVisual(display, 0);
    window = XCreateSimpleWindow(display, RootWindow(display, 0), 0, 0, width,
        height, 1, 0, 0);
    ximage = XCreateImage(display, visual, 24, ZPixmap, 0, 
        reinterpret_cast<char *>(image), width, height, 8, 0);
    XMapWindow(display, window);
  }

  std::unique_ptr<cluon::SharedMemory> sharedMemory{
    new cluon::SharedMemory{name}};
  if (sharedMemory && sharedMemory->valid()) {
    std::clog << argv[0] << ": Attached to shared memory '"
      << sharedMemory->name() << " (" << sharedMemory->size() << " bytes)."
      << std::endl;

    uint32_t const n = 300;
    std::array<double, n> times;
    double timeSum{0};

    for (uint32_t k{0}; k < n; k++) {
      int32_t *payload;
      {
        VkResult res = vkMapMemory(device, memory, 0, memorySize, 0,
            reinterpret_cast<void **>(&payload));
        if (res != VK_SUCCESS) {
          std::cerr << "Could not map GPU memory" << std::endl;
          destroyVulkan();
          return -1;
        }
      }
      sharedMemory->wait();
      sharedMemory->lock();

      cluon::data::TimeStamp start = cluon::time::now();
      
      memcpy(payload, sharedMemory->data(), bufferSize);
      sharedMemory->unlock();

      vkUnmapMemory(device, memory);

      VkQueue queue;
      {
        vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue);
        VkSubmitInfo const submitInfo = {
          VK_STRUCTURE_TYPE_SUBMIT_INFO,
          0,
          0,
          0,
          0,
          1,
          &commandBuffer,
          0,
          0
        };
        VkResult res = vkQueueSubmit(queue, 1, &submitInfo, 0);
        if (res != VK_SUCCESS) {
          std::cerr << "Could not submit queue" << std::endl;
          destroyVulkan();
          return -1;
        }
      }

      {
        VkResult res = vkQueueWaitIdle(queue);
        if (res != VK_SUCCESS) {
          std::cerr << "Could not submit queue" << std::endl;
          destroyVulkan();
          return -1;
        }
      }

      {
        VkResult res = vkMapMemory(device, memory, 0, memorySize, 0,
            reinterpret_cast<void **>(&payload));
        if (res != VK_SUCCESS) {
          std::cerr << "Could not submit queue" << std::endl;
          destroyVulkan();
          return -1;
        }
      }

      double time = (cluon::time::toMicroseconds(cluon::time::now())
          - cluon::time::toMicroseconds(start)) / 1000.0;
      std::cout << "Time per frame: " << time << " ms (k=" << +k << " of " 
        << n << ")" << std::endl;

      times[k] = time;
      timeSum += time;
      
      if (verbose) {
        memcpy(image, reinterpret_cast<uint8_t *>(payload) + bufferSize, bufferSize);
        XPutImage(display, window, DefaultGC(display, 0), ximage, 0, 0, 0, 0,
            width, height);
      }
      
      vkUnmapMemory(device, memory);
    }

    double mean{timeSum / n};
    double variance{0.0};
    {
      double tmp{0};
      for (uint32_t k{0}; k < n; k++) {
        tmp += (times[k] - mean) * (times[k] - mean);
      }
      variance = tmp / (n - 1);
    }
    std::cout << "Time per frame: mean " << mean << " ms, std " 
      << std::sqrt(variance) << std::endl;
  }

  destroyVulkan();

  return 0;
}
